import * as esbuild from 'esbuild-wasm';
import { Console } from 'node:console';
import { useState, useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';

const App = () => {
    const ref = useRef<any>();
    const [input, setInput] = useState('');
    const [code, setCode] =  useState('');

    const startService = async () => {
        await esbuild.initialize({
            worker: true,
            wasmURL: '/esbuild.wasm'
        });
        ref.current = true;
        //console.log(service);
    };
    useEffect(() => {
        startService();
    },[])

    const onClick = async () => {
        if(!ref.current){
            return;
        }

        const result = await esbuild.transform(input, {
            loader: 'jsx',
            target: 'es2015'
        })

        setCode(result.code);
    }

    return <div>
        <textarea value={input} onChange={e => setInput(e.target.value)}></textarea>
        <div>
            <button onClick={onClick}>Submit</button>
        </div>
        <pre>{code}</pre>
    </div>
};

ReactDOM.render(
    <App/>,
    document.querySelector('#root')
);